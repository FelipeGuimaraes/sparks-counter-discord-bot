drop table if exists fixas;
create table fixas (
    mixer text primary key,
    nick text not null,
    sparks integer
);

drop table if exists full;
create table full (
    mixer text primary key,
    nick text not null,
    sparks integer
);