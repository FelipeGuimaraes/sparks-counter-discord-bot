import discord
import asyncio
from os import path
import sqlite3 as sql
import requests

bot_token = ''
channel_id = ''   # canal que o bot ira mandar o total de sparks
server_id = ''    # id do server

client = discord.Client()

# Insere contribuicao integral ou fixa
# Atualiza valor da contribuicao fixa se ja existir
async def add_contribution(mixer, name, value=None):
    ROOT = path.dirname(path.relpath((__file__)))
    conn = sql.connect(path.join(ROOT, 'database.db'))
    cur = conn.cursor()

    session = requests.Session()
    channel_response = session.get('https://mixer.com/api/v1/channels/{}'.format(mixer))
    try:
        channel_response.json()['user']
    except:
        result = f'O usuário {mixer} não foi encontrado.'
        conn.close()
        return result

    if value == None:
        try:
            cur.execute('INSERT INTO full values(?, ?, 0);', (mixer, name))
            result = f'A contribuição integral de {mixer} foi contabilizada com sucesso.\nMuito obrigado pela colaboração!'
            conn.commit()
            await update()
        except:
            result = f'Apenas uma contribuição por conta da mixer é permitida'
    else:
        cur.execute('SELECT * FROM fixas WHERE mixer = ?;', (mixer,))
        row = cur.fetchone()

        if row == None:
            cur.execute('INSERT INTO fixas values(?, ?, ?);', (mixer, name, value))
            result = f'A contribuição de {mixer} no valor de {value} foi contabilizada com sucesso.\nMuito obrigado pela colaboração!'
            conn.commit()
        else:
            cur.execute('UPDATE fixas SET sparks = ? WHERE mixer = ?;', (value, mixer))
            result = f'A contribuição de {mixer} foi atualizada para o valor de {value} sparks.'
            conn.commit()
    
    conn.close()
    return result

# Atualiza os sparks das contribuicoes integrais, usando a API da mixer
async def update():
    session = requests.Session()
    ROOT = path.dirname(path.relpath((__file__)))
    conn = sql.connect(path.join(ROOT, 'database.db'))
    cur = conn.cursor()
    cur.execute('SELECT mixer FROM full;')
    result = cur.fetchall()
    
    for row in result:
        if row[0] == 'aux':
            continue
        
        channel_response = session.get('https://mixer.com/api/v1/channels/{}'.format(row[0]))
        spark_user = channel_response.json()['user']['sparks']
        cur.execute('UPDATE full SET sparks = ? WHERE mixer = ?;', (spark_user, row[0]))
        conn.commit()

    cur.execute('SELECT SUM(sparks) FROM full;')
    full = cur.fetchone()

    cur.execute('SELECT SUM(sparks) FROM fixas;')
    fixa = cur.fetchone()

    conn.close()

    total = full[0] + fixa[0]

    # Comente as linhas 83-85 APENAS na primeira execução do comando '!atualizar'
    # Em seguida pegue o id da msg enviada pelo bot e coloque na variavel msg_id
    msg_id = ''
    channel = client.get_channel(channel_id)
    msg = await client.get_message(channel, msg_id)
    await client.edit_message(msg, f'Temos {total} sparks para serem enviados')

    # Descomente a linha abaixo na primeira execução. Mantenha comentada nas demais execuções
    #await client.send_message(channel, f'Temos {total} sparks serem enviados.')

# Remove todas as contribuicoes de um usuario do discord
def remove(nick):
    ROOT = path.dirname(path.relpath((__file__)))
    conn = sql.connect(path.join(ROOT, 'database.db'))
    cur = conn.cursor()
    
    cur.execute('SELECT mixer FROM full WHERE nick = ?;', (nick,))
    rows_full = cur.fetchall()

    cur.execute('SELECT mixer FROM fixas WHERE nick = ?;', (nick,))
    rows_fixas = cur.fetchall()

    cur.execute('DELETE FROM full WHERE nick = ?', (nick,))
    cur.execute('DELETE FROM fixas WHERE nick = ?', (nick,))
    conn.commit()
    conn.close()

    rows_full.extend(rows_fixas)
    return rows_full

async def set_to_ready(user):
    role = discord.utils.get(client.get_server(server_id).roles, name='Pronto')
    await client.add_roles(user, role)

async def clear(role_name):
    server = client.get_server(server_id)
    role = discord.utils.get(client.get_server(server_id).roles, name=role_name)
    for user in server.members:
        if role in user.roles:
            await client.remove_roles(user, role)


@client.event
async def on_ready():
    print('BOTera 50M is online!')

@client.event
async def on_message(message):
    if message.author.bot == True: # Desconsidera mensagens do bot
        return
    if (message.content == '.qq'): # Close bot
        print(message.author.name + ' finished the BOT.')
        await client.logout()
        await client.close()
        exit(1)

    #Contribuir
    if message.content.startswith('!contribuir'):
        print(message.author.name + ' used ' + message.content) # Console output

        channel = message.channel

        if channel.is_private == True:
            msg = message.content.split(' ')
            if len(msg) == 3:
                output = await add_contribution(msg[1], message.author.name, msg[2])
            else:
                output = await add_contribution(msg[1], message.author.name)
            
            await client.send_message(channel, output)
            await update()
        else:
            await client.send_message(channel, 'Esse comando só está disponível via mensagem privada comigo.')

    # Remover
    elif message.content == '!remover':
        print(message.author.name + ' used ' + message.content) # Console output

        flag_first = False
        channel = message.channel
        
        if channel.is_private == True:
            accounts = remove(message.author.name)

            if len(accounts) > 1:
                output = 'As contribuições de '
                
                for i in accounts:
                    if flag_first == False:
                        flag_first = True
                    else:
                        output += ', '
                    output += f'\"{i[0]}\"'
                output += ' foram removidas com sucesso.'
            
            elif len(accounts) > 0:
                output = f'A contribuição de {accounts[0][0]} foi removida com sucesso.'
            else:
                output = 'Voce nao tem nenhuma contribuição associada ao seu usuário do discord.'
            
            await client.send_message(channel, output)
            await update()
        else:
            await client.send_message(channel, 'Esse comando só está disponível via mensagem privada comigo.')
    
    # Atualizar
    elif message.content == '!atualizar':
        print(message.author.name + ' used ' + message.content) # Console output

        await update()
        await client.send_message(message.channel, f'O total de sparks foi atualizado em <#{channel_id}>.')
    
    # Pronto
    elif message.content == '!pronto':
        print(message.author.name + ' used ' + message.content) # Console output

        user = message.author
        await set_to_ready(user)
    
    #Clear
    elif message.content.startswith('!clear') == True:
        if message.author.server_permissions.administrator == True:
            msg = message.content.split(' ', 1)
            await clear(msg[1])
            await client.send_message(message.channel, f'Todos tiveram o cargo \"{msg[1]}\" removido.')
        else:
            await client.send_message(message.channel, f'Apenas administradores do servidor podem usar este comando.')
    
    # Comandos
    elif message.content == '!comandos':
        print(message.author.name + ' used ' + message.content) # Console output
        mensagem = '- !contribuir "nick da mixer" (sem aspas) - VIA MENSAGEM PRIVADA COMIGO\n'
        mensagem += 'Adiciona uma contribuição considerando todos os sparks da sua conta (atualiza conforme você farma sparks).\n\n'
        mensagem += '- !contribuir "nick da mixer" "quantidade" (sem aspas) - VIA MENSAGEM PRIVADA COMIGO\n'
        mensagem += 'Adiciona uma contribuição com um valor fixo de sparks.\n'
        mensagem += 'PS: Esse comando também pode ser utilizado para alterar a "quantidade" de uma contribuição feita anteriormente.\n\n'
        mensagem += '- !remover - VIA MENSAGEM PRIVADA COMIGO\n'
        mensagem += 'Remove todas as contribuições associadas ao seu usuário do discord.\n\n'
        mensagem += '- !atualizar\n'
        mensagem += f'Atualiza o total de sparks em <#{channel_id}>'

        await client.send_message(message.channel, mensagem)

client.run(bot_token)